#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n, q;
    cin >> n >> q;
    cin.ignore();
    vector<vector<int>> a(n);

    for (int i = 0; i < n; i++) {
        string line;
        getline(cin, line);
        istringstream ss(line);

        //first number is the size of k
        int k_size, k_item;
        ss >> k_size;
        vector<int> k(k_size, 0);
        //populate k array
        for (int j = 0; j < k_size; j++) {
            ss >> k_item;
            //dont use push_back()
            k[j] = k_item;
        }
        //add k to the array a. dont use push_back().
        a[i] = k;
    }
    for (int i = 0; i < q; i++)
    {
        string query;
        getline(cin, query);
        istringstream ss(query);

        //get the location in the vector
        int x, y;
        ss >> x >> y;
        cout << a.at(x).at(y) << endl;
    }
    return 0;
}
