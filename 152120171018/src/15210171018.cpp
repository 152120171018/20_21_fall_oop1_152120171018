#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

double Sum(int Arr[], int range);
long long int Product(int Arr[], int range);

int main()
{
	/*!
	*	Reading from the input.txt file.
	*	For this: Reading is done with ifstream.
	*	Printing the addition, multiplication, minimum and average value with the if else statement.
	*/
	int* Arr, range;
	ifstream readingFile("input.txt");
	if (!readingFile) {
		cout << "The file was not opened.\n";
	}
	else {
		readingFile >> range;
		if ((range >= 'a' && range <= 'z') || (range >= 'A' && range <= 'Z') || range <= 0) {
			cout << "YOU TRIED TO CREATE THE RANGE AT THE INCORRECT VALUE !!!" << endl;
			cout << "Please do not create a range of zero and a value less than zero." << endl;
			cout << "Also, please use numbers when specifying a range.Space is not created with letters." << endl;
			return -1;
		}
		else {
			Arr = new int[range];
			for (int i = 0; i < range; i++) readingFile >> Arr[i];
			cout << "Sum is " << Sum(Arr, range) << endl;
			cout << "Product is " << Product(Arr, range) << endl;
			double average = 0;
			average = Sum(Arr, range) / range;
			cout << "Average is " << average << endl;
		}
	}
	cout << endl;
	return 0;
	system("pause");
}

/*!
*		--------------------------------------------- Sum ---------------------------------------------
*		Each element in the array is collected in this function.
*		We have created the function as double to use the sum in the main function.
*/
double Sum(int Arr[], int range) {
	if ((range >= 'a' && range <= 'z') || (range >= 'A' && range <= 'Z') || range <= 0)
		return -1;
	double sum = 0;
	for (int i = 0; i < range; i++) sum += Arr[i];
	return sum;
}

/*!
*		--------------------------------------------- Product ---------------------------------------------
*		Each element in the array is multiplied by this function.
*		We have created the function as long long int to use the sum in the main function.
*
*/
long long int Product(int Arr[], int range) {
	if ((range >= 'a' && range <= 'z') || (range >= 'A' && range <= 'Z') || range <= 0)
		return -1;
	long long int product = 1;
	for (int i = 0; i < range; i++) product *= Arr[i]; // product the numbers in the array
	return product;
}